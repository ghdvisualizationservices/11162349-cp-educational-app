﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObjectIsolate : MonoBehaviour
{
    public GameObject isolateObject;
    public bool isolated;

    private ModelControl mc;

    [HideInInspector]
    public Vector3 originalPos;

    [HideInInspector]
    public Vector3 originalRot;

    [HideInInspector]
    public Quaternion originalRote;

    [HideInInspector]
    public Quaternion isolateRote;

    public bool enlarge;

    [HideInInspector]
    public bool isEnlarged;
    public float enlargeValue = 1.5f;

    public Vector3 isolatePos;
    public Vector3 isolateRot;

    private Material isolatedPartMat;

    private void Start()
    {
        originalPos = transform.localPosition;
        originalRot = transform.localEulerAngles;
        originalRote = transform.localRotation;
        
        mc = FindObjectOfType<ModelControl>();
    }

    public void IsolateObject()
    {
        Transform parent = isolateObject.transform.parent;

        for (int i = 0; i < parent.childCount; i ++)
        {
            GameObject obj = parent.GetChild(i).gameObject;
            if (!obj.name.Equals(isolateObject.name))
            {
                obj.transform.SetParent(Camera.main.transform);
                i--;

                if (obj.GetComponent<MeshRenderer>() != null)
                {
                    StartCoroutine(mc.FadeMaterial(obj, 1));
                }

                // Sub meshes
                for (int j = 0; j < obj.transform.childCount; j ++)
                {
                    if (obj.transform.GetChild(j).GetComponent<MeshRenderer>() != null)
                    {
                        StartCoroutine(mc.FadeMaterial(obj.transform.GetChild(j).gameObject, 1));
                    }

                    if (j == 0)
                    {
                        Material m = Instantiate(Resources.Load("Transparent")) as Material;
                        obj.GetComponentInChildren<LineRenderer>().material = m;
                        obj.GetComponentInChildren<Button>().enabled = false;
                        StartCoroutine(mc.FadeLabel(1, obj.transform.GetChild(0).GetChild(0).gameObject));
                    }
                    else if (j == obj.transform.childCount - 1)
                    {
                        for (int k = 0; k < obj.transform.GetChild(j).childCount; k ++)
                        {
                            if (obj.transform.GetChild(j).GetChild(k).GetComponent<MeshRenderer>() != null)
                                StartCoroutine(mc.FadeMaterial(obj.transform.GetChild(j).GetChild(k).gameObject, 1));
                        }
                    }
                }
            }
            // Matched
            else
            {
                StartCoroutine(LerpToVector(obj.transform, true));

                GameObject.Find("Model_Name_Text").GetComponent<Text>().text = mc.model.name + " - " + obj.name;

                if (enlarge)
                {
                    isEnlarged = true;
                }

                Material m = Instantiate(Resources.Load("Transparent")) as Material;
                Material n = Instantiate(Resources.Load("Transparent")) as Material;
                obj.transform.GetChild(0).GetComponentInChildren<LineRenderer>().material = m;
                StartCoroutine(mc.FadeLabel(1, obj.transform.GetChild(0).GetChild(0).gameObject));

                for (int k = 0; k < obj.transform.GetChild(1).childCount; k ++)
                {
                    obj.transform.GetChild(1).GetChild(k).GetComponentInChildren<LineRenderer>().material = n;
                    StartCoroutine(mc.FadeLabel(0, obj.transform.GetChild(1).GetChild(k).gameObject));
                }

                mc.isolatingObject = true;
                StartCoroutine(Camera.main.GetComponent<MouseOrbit>().ResetCamera());
                isolated = true;
                StartCoroutine(mc.WaitForTransitionShowModelButton(true));
            }
        }
    }

    public IEnumerator LerpToVector (Transform t, bool iso)
    {
        float time = 0;
        while (t.localPosition != (iso ? isolatePos : originalPos))
        {
            time += Time.deltaTime;
            t.localPosition = Vector3.Lerp(t.localPosition, iso ? isolatePos : originalPos, time);
            t.localRotation = Quaternion.Lerp(t.localRotation, iso ? isolateRote : originalRote, time);
            t.localScale = Vector3.Lerp(t.localScale, iso ?
                new Vector3(enlargeValue, enlargeValue, enlargeValue) :
                Vector3.one, time);
            yield return null;
        }
    }

    public void IsolatePart(GameObject label)
    {
        Transform parent = label.transform.parent;
        for (int i = 0; i < parent.childCount; i ++)
        {
            if (!parent.GetChild(i).name.Equals(label.name))
            {
                Material m = Instantiate(Resources.Load("Transparent")) as Material;
                parent.GetChild(i).GetComponentInChildren<LineRenderer>().material = m;
                parent.GetChild(i).GetComponentInChildren<Button>().enabled = false;
                StartCoroutine(mc.FadeLabel(1, parent.GetChild(i).gameObject));
            }
            else
            {
                parent.GetChild(i).GetComponentInChildren<Button>().enabled = false;

                int childPos = i;
                GameObject.Find("Labels_Toggle").GetComponent<Toggle>().interactable = false;
                //GameObject.Find("Change_Model_Dropdown").GetComponent<Dropdown>().interactable = false;
                mc.showWholeModelButton.gameObject.SetActive(false);
                GameObject.Find("Isolate_Panel").GetComponent<Animator>().SetBool("In", true);

                Material partMat = GameObject.Find(label.GetComponent<LabelInfo>().labelName).GetComponent<MeshRenderer>().material;
                isolatedPartMat = Instantiate(partMat);
                partMat.color = Color.red;
                partMat.SetTexture("_MainTex", null);

                SetIsolateInfo(parent, i, label);
            }
        }
    }

    public void CloseIsolatePart(GameObject label)
    {
        StopAllCoroutines();

        Transform parent = label.transform.parent;
        for (int i = 0; i < parent.childCount; i++)
        {
            parent.GetChild(i).GetComponentInChildren<Button>().enabled = true;

            if (!parent.GetChild(i).name.Equals(label.name))
            {
                StartCoroutine(mc.FadeLabel(0, parent.GetChild(i).gameObject));
                GameObject.Find("Isolate_Panel").GetComponent<Animator>().SetBool("In", false);
            }
            else
            {
                GameObject.Find(label.GetComponent<LabelInfo>().labelName).GetComponent<MeshRenderer>().material = isolatedPartMat;
                isolatedPartMat = null;
            }

            /*if (!parent.GetChild(i).name.Equals(label.name))
            {
                StartCoroutine(mc.FadeLabel(0, parent.GetChild(i).gameObject));
                GameObject.Find("Isolate_Panel").GetComponent<Animator>().SetBool("In", false);
            }
            else
            {
                GameObject.Find(label.GetComponent<LabelInfo>().labelName).GetComponent<MeshRenderer>().material = isolatedPartMat;
                isolatedPartMat = null;
            } */
        }

        GameObject.Find("Labels_Toggle").GetComponent<Toggle>().interactable = true;
        //GameObject.Find("Change_Model_Dropdown").GetComponent<Dropdown>().interactable = true;
        StartCoroutine(mc.WaitForTransitionShowModelButton(true));
    }

    public void NextPreviousIsolatePart(int nextPrev, int childPos, GameObject label)
    {
        Transform parent = label.transform.parent;

        int newPos = childPos + nextPrev;

        if (newPos == -1)
            newPos = parent.childCount - 1;
        else if (newPos == parent.childCount)
            newPos = 0;

        // Fade current label
        Material m = Instantiate(Resources.Load("Transparent")) as Material;
        parent.GetChild(childPos).GetComponentInChildren<LineRenderer>().material = m;
        StartCoroutine(mc.FadeLabel(1, parent.GetChild(childPos).gameObject));

        // Make it glow
        GameObject.Find(parent.GetChild(childPos).GetComponent<LabelInfo>().labelName).GetComponent<MeshRenderer>().material = isolatedPartMat;
        isolatedPartMat = null;

        Material partMat = GameObject.Find(parent.GetChild(newPos).GetComponent<LabelInfo>().labelName).GetComponent<MeshRenderer>().material;
        isolatedPartMat = Instantiate(partMat);
        partMat.color = Color.red;
        partMat.SetTexture("_MainTex", null);

        // -1 = prev, 1 = next
        StartCoroutine(mc.FadeLabel(0, parent.GetChild(newPos).gameObject));

        SetIsolateInfo(parent, newPos, label);
    }

    private void SetIsolateInfo(Transform parent, int pos, GameObject label)
    {
        GameObject.Find("Isolate_Close").GetComponent<Button>().onClick.RemoveAllListeners();
        GameObject.Find("Isolate_Next").GetComponent<Button>().onClick.RemoveAllListeners();
        GameObject.Find("Isolate_Previous").GetComponent<Button>().onClick.RemoveAllListeners();

        GameObject.Find("Isolate_Name").GetComponent<Text>().text = parent.GetChild(pos).GetComponent<LabelInfo>().labelName;
        GameObject.Find("Isolate_Desc").GetComponent<Text>().text = parent.GetChild(pos).GetComponent<LabelInfo>().labelDescription;
        GameObject.Find("Isolate_Close").GetComponent<Button>().onClick.AddListener(delegate { CloseIsolatePart(parent.GetChild(pos).gameObject); });
        GameObject.Find("Isolate_Next").GetComponent<Button>().onClick.AddListener(delegate { NextPreviousIsolatePart(1, pos, label); });
        GameObject.Find("Isolate_Previous").GetComponent<Button>().onClick.AddListener(delegate { NextPreviousIsolatePart(-1, pos, label); });
    }
}
